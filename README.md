# Contur

Contur is a procedure and toolkit designed to probe theories Beyond the
Standard Model using measurements at particle colliders. The original procedure
is defined in a
[white paper](https://inspirehep.net/literature/1470949)
, which should be used as a reference for the method, with an updated manual [here](https://inspirehep.net/literature/1845411).
The statistical approach is also explained in a [`readme`](contur/factories/Readme.md).

Results are available [here](https://hepcedar.gitlab.io/contur-webpage/index.html). This README
concentrates on how to get Contur running yourself.

We also have a support mailing list where volunteer developers will do their best to answer your questions: contur-support@cern.ch

## In-code documentation

The in-code documentation, processed via Sphinx in the [doc](doc) directory, is available [here](https://hepcedar.gitlab.io/contur/).

## Getting started

[Installation](doc/READMEs/installation.md)

[First contur run](doc/READMEs/firstrun.md)

## Exclusion plots
[Generating your first 2D heatmap](doc/READMEs/exclusions2D.md)

[Generating a heatmap using MadGraph](doc/READMEs/madgraph.md)

[Using machine-learning assisted parameter scanning](doc/READMEs/oracle.md)

## Adding new Rivet analyses

[Adding a new analysis from your current Rivet release](data/DB/README.md)

[Adding an new or modified Rivet analysis locally](data/Rivet/README.md)

## Developer information
See the [Developers page](doc/READMEs/developers.md).

## Docker 

Note that Dockerfiles containing a working setup (with or without Herwig) are available in the [docker subdirectory](docker).
Instructions for how best to use Contur from these are still in preparation, but you can now get the latest by doing
`docker pull hepstore/contur`. The Rivet/Yoda docker setup is quite mature and the guidance
[here](https://gitlab.com/hepcedar/rivet/-/blob/release-3-1-x/doc/tutorials/docker.md)
might help.

## Open projects
There is a [list of open projects](https://gitlab.com/hepcedar/contur/-/wikis/Open-and-ongoing-projects) where we always welcome contributions!

# Directory structure

### [`contur`](https://hepcedar.gitlab.io/contur/contur.html)

The main source code and template run area.

### [`data`](data/README.md)

Common area for all data files (models, modified rivet elements, SM predictions etc)

### [`tests`](tests)

To run contur's code tests, do `make check` from the top directory, or to use other pytest options, `cd` into `tests` and run `$ pytest`.
