"""

The Depot module contains the Depot class. This is intended to be the high level analysis control, 
most user access methods should be implemented at this level

"""

import os
import pickle
import numpy as np
import sqlite3 as db 
import scipy.stats as spstat
import contur
import contur.factories.likelihood as lh
import contur.config.config as cfg
import contur.util.utils as cutil
from contur.factories.yoda_factories import YodaFactory
from contur.factories.likelihood_point import LikelihoodPoint
from contur.data.data_access_db import write_grid_data
from contur.data.data_access_db import open_for_reading
import contur.plot.yoda_plotting as cplot
from yoda.plotting import script_generator




class Depot(object):
    """ Parent analysis class to initialise

    This can be initialised as a blank canvas, then the desired workflow is to add parameter space points to the Depot using
    the :func:`add_point` method. This appends each considered point to the objects internal :attr:`points`. To get the point 
    message from database rather than map file to the Depot using the :func:`add_points_from_db` method. 

    Path for writing out objects is determined by cfg.plot_dir

    """

    def __init__(self):

        self._point_list = []

    def write(self, outDir, args, yodafile=None):
        """Function to write depot information to disk
        
        write a results db files to outDir
        if cfg.mapfile is not None, also write out a "map" file containing the full pickle of this depot instance
        if cfg.csvfile is not None, also write out a csv file containing the data

        :param outDir:
            String of filesystem location to write out the pickle of this instance to
        :type outDir: ``string``

        """
        cutil.mkoutdir(outDir)

        # populate the local database for this grid
        try:
            write_grid_data(self,args,yodafile)
        except cfg.ConturError as ce:
            cfg.contur_log.error("Failed to write results database. Error was: {}".format(ce))

        if cfg.mapfile is not None:
            path_out = os.path.join(outDir,cfg.mapfile)
            cfg.contur_log.info("Writing output map to : " + path_out)

            with open(path_out, 'wb') as f:
                pickle.dump(self, f, protocol=2)

        if cfg.csvfile is not None:
            path_out = os.path.join(outDir,cfg.csvfile)
            cfg.contur_log.info("Writing output csv to : " + path_out)

            self.export(path_out, include_dominant_pools=True, include_per_pool_cls=True)

    def add_point(self, yodafile, param_dict):
        """
        Add yoda file and the corresponding parameter point into the depot
        """

        try:
            yFact = YodaFactory(yodaFilePath=yodafile)
            lh_point = LikelihoodPoint(yodaFactory=yFact, paramPoint=param_dict)
            lh_point.set_run_point(cfg.runpoint)

        except cfg.ConturError as ce:
            cfg.contur_log.warning(ce)
            cfg.contur_log.warning("Skipping file.")
            return

        for stat_type in cfg.stat_types:
            
            # get test statistics for each block
            try:
                lh.likelihood_blocks_find_dominant_ts(lh_point.likelihood_blocks,stat_type)

                # get cls for each block
                lh.likelihood_blocks_ts_to_cls(lh_point.likelihood_blocks,stat_type)
                
            except ValueError:
                cfg.contur_log.warn("No likelihoods present for {}".format(stat_type))
                

        obs_excl_dict = {}
            
        for observable in yFact.observables:
            
            exclusions = {}
            for stat_type in cfg.stat_types:
                exclusions[stat_type] = observable.likelihood.getCLs(stat_type)
            obs_excl_dict[observable.signal.path()] = exclusions

            # write out the plotting scripts here, now CLs have been calculated
            # if the databg exclusion is None, this means there were no BSM entries, so do not write the script
            if not cfg.silenceWriter and exclusions[cfg.databg] is not None:
                        
                # create output dir for each analysis
                if not cfg.gridMode :
                    plotsOutdir = os.path.join(cfg.plot_dir, observable.pool, observable.analysis.name)
                else:
                    plotsOutdir = os.path.join(cfg.plot_dir, cfg.runpoint, observable.pool, observable.analysis.name)
                cutil.mkoutdir(plotsOutdir)

                # get YODA objects for BSM + SM, theory and data + BG
                thisPathYodaFiles = cplot.createYODAPlotObjects(observable, nostack=cfg.nostack)

                # get plot contents, pass YODA dict and reference data YODA object.
                plotContents = cplot.assemble_plotting_data(observable, thisPathYodaFiles)

                # create scripts
                pyScript = script_generator.process(plotContents, observable.signal.path(), plotsOutdir.rsplit('/',1)[0], ["PDF", "PNG"])
                            
        lh_point.obs_excl_dict = obs_excl_dict

        # combine subpools (does it for all test stats, but has to be done after we have found the dominant bin for each test stat (above)
        lh.combine_subpool_likelihoods(lh_point.likelihood_blocks, omitted_pools="")

        for stat_type in cfg.stat_types:

            try:
                # sort the blocks according to this test statistic
                lh_point.set_sorted_likelihood_blocks(lh.sort_blocks(lh_point.likelihood_blocks,stat_type),stat_type)
                lh_point.set_full_likelihood(stat_type,lh.build_full_likelihood(lh_point.get_sorted_likelihood_blocks(stat_type),stat_type))
                
                if lh_point.get_full_likelihood(stat_type) is not None:
                    cfg.contur_log.info(
                        "Added yodafile with reported {} exclusion of: {} ".format(stat_type,str(lh_point.get_full_likelihood(stat_type).getCLs())))
                    lh_point.fill_pool_dict(stat_type)
                
                else:
                    cfg.contur_log.info("No {} likelihood could be evaluated".format(stat_type))
        
            except ValueError:
                cfg.contur_log.warn("Not adding likelihood for {}".format(stat_type))

        try:
            lh_point.set_run_point(cfg.runpoint.split('/')[-1])
        except:
            lh_point.set_run_point(cfg.runpoint)
        cfg.contur_log.info("adding point {}".format(lh_point))
        self._point_list.append(lh_point)

        
    def add_points_from_db(self, file_path):
        """
        Get the info of model points from the result database into the depot class


        @TODO write a "get from DB" method for likelihood_point?
        """

        cfg.results_dbfile = os.path.join(os.getcwd(), file_path)
        conn = open_for_reading(cfg.results_dbfile)
                
        c = conn.cursor()
        # start with map_id to select all related data info in a run
        id = c.execute("select id from map").fetchall()
        for map_id in id:
            map_id = map_id[0]

            # model point maybe should have a map id?            
            #  model_point_list = c.execute("select id from model_point where map_id = {};".format(map_id)).fetchall()
            model_point_list = c.execute("select id, yoda_files from model_point;").fetchall()

            for model_point in model_point_list:

                model_point_id = model_point[0]
                yoda_files = model_point[1]
                
                # set a flag to avoid reading param_point repeatedly in the same point
                likelihood_point = LikelihoodPoint()
                param_point = {}
                search_sql1 = "select name from parameter_value where model_point_id =" + str(model_point_id) + ";"
                name = c.execute(search_sql1).fetchall()
                search_sql2 = "select value from parameter_value where model_point_id =" + str(model_point_id) + ";"
                value = c.execute(search_sql2).fetchall()
                # store parameter name and value in a dicionary
                for index, point_name in enumerate(name):
                    param_point[point_name[0]] = value[index][0]

                # select all run_id which map_id and model_point_id are current
                run_list = c.execute("select id,stat_type,combined_exclusion from run where model_point_id = {} and map_id = {};".format(model_point_id,map_id)).fetchall()

                obs_excl_dict = {}
                # not ideal that each stat_type is its own run...
                for run in run_list:
                    run_id = run[0]
                    stat_type = run[1]
                    combined_exclusion = run[2]

                    pool_name = c.execute("select pool_name from exclusions where run_id =" + str(run_id) + ";").fetchall()
                    exclusion = c.execute("select exclusion from exclusions where run_id =" + str(run_id) + ";").fetchall()
                    histos = c.execute("select histos from exclusions where run_id =" + str(run_id) + ";").fetchall()
                    # use a dictionary to store pool name and exclusion result in each run
                    pool_exclusion = {}
                    for index, name in enumerate(pool_name):
                        pool_exclusion[name[0]] = exclusion[index][0]

                    # use a dictionary to store pool name and histos result in each run
                    pool_histos = {}
                    for index, name in enumerate(pool_name):
                        pool_histos[name[0]] = histos[index][0]

                    pool_name = c.execute("select pool_name from intermediate_result where run_id =" + str(run_id) + ";").fetchall()
                    ts_b = c.execute("select ts_b from intermediate_result where run_id =" + str(run_id) + ";").fetchall()
                    ts_s_b = c.execute("select ts_s_b from intermediate_result where run_id =" + str(run_id) + ";").fetchall()
                    # use the dictionaries to store pool name and ts_b/ts_s_b result in each run
                    pool_ts_b = {}
                    pool_ts_s_b = {}
                    for index, name in enumerate(pool_name):
                        pool_ts_b[name[0]] = ts_b[index][0]
                        pool_ts_s_b[name[0]] = ts_s_b[index][0]

                    # get the per-histo exclusions
                    rows = c.execute("select histo, stat_type, exclusion from obs_exclusions where run_id = {};".format(run_id)).fetchall()
                    for row in rows:
                        if not row[0] in obs_excl_dict.keys():
                            obs_excl_dict[row[0]] = {}
                        obs_excl_dict[row[0]][row[1]] = row[2]
                        
                    # use a list to store each data type in a map
                    likelihood_point.store_point_info(stat_type, combined_exclusion, pool_exclusion, pool_histos, pool_ts_b, pool_ts_s_b, obs_excl_dict, yoda_files)

                # add the runpoint info ("BEAM/POINT") into the individual points (if present)
                try:
                    runpoint_id = c.execute("select run_point from model_point;").fetchall()
                    likelihood_point.set_run_point(runpoint_id[int(model_point_id) - 1])
                except:
                    cfg.contur_log.warn("Problem reading runpoint attribute from DB. This may be an old (<2.5) results file.")
                    pass
                    
                likelihood_point.store_param_point(param_point)

                # add the likelihood point into the point list
                self._point_list.append(likelihood_point)
                
        conn.commit()
        conn.close()

    def resort_points(self):
        """Function to trigger rerunning of the sorting algorithm on all items in the depot, 
        typically if this list has been affected by a merge by a call to :func:`contur.depot.merge`
        """

        for p in self.points:
            for stat_type in cfg.stat_types:
                p.resort_blocks(stat_type)

    def merge(self, depot):
        """
        Function to merge this conturDepot instance with another.
        
        Points with identical parameters will be combined. If point from the input Depot is not present in this Depot,
        it will be added.

        :param depot:
            Additional instance to conturDepot to merge with this one
        :type depot: :class:`contur.conturDepot`


        """
        new_points = []
        for point in depot.points:

            merged = False

            # look through the points to see if this matches any.
            for p in self.points:

                if not merged:
                    same = True
                    valid = True
                    for parameter_name, value in p.param_point.items():
                        try:
                            # we don't demand the auxilliary parameters match, since they can be things like
                            # cross secitons, which will depend on the beam as well as the model point.
                            if point.param_point[parameter_name] != value and not parameter_name.startswith("AUX:"):
                                same = False
                                break
                        except KeyError:
                            cfg.contur_log.warning("Not merging. Parameter name not found:" + parameter_name)
                            valid = False

                    # merge this point with an existing one
                    if same and valid:
                        cfg.contur_log.debug("Merging {} with {}".format(point.param_point,p.param_point))
                        p.pool_exclusion_dict.update(point.pool_exclusion_dict)
                        p.pool_histos_dict.update(point.pool_histos_dict)
                        p.pool_ts_b.update(point.pool_ts_b)
                        p.pool_ts_s_b.update(point.pool_ts_s_b)
                        p.obs_excl_dict.update(point.obs_excl_dict)
                        #       self.combined_exclusion_dict = {} (does this get updated?)

                        for stat_type in cfg.stat_types:
                            try:
                                blocks = p.get_sorted_likelihood_blocks(stat_type)
                                blocks.extend(point.get_sorted_likelihood_blocks(stat_type))
                                #p.obs_excl_dict.update(point.obs_excl_dict)
       
                                cfg.contur_log.debug("Previous CLs: {} , {}".format(point.get_full_likelihood(stat_type).getCLs(),p.get_full_likelihood(stat_type).getCLs()))
                            except AttributeError:
                                # This happens when no likelihood was evaluated for a particular block, so
                                # we can't query it for a CLs...
                                pass

                        merged = True

            # this is a new point
            if not merged:
                new_points.append(point)
                cfg.contur_log.debug("Adding new point {} with dominant.".format(point.param_point))
                

        if len(new_points)>0:
            cfg.contur_log.debug("Adding {} new points to {}".format(len(new_points),len(self.points)))
            self.points.extend(new_points)


    def _build_frame(self, include_dominant_pools=False, include_per_pool_cls=False):
        """:return pandas.DataFrame of the depot points"""
        try:
            import pandas as pd
        except ImportError:
            cfg.contur_log.error("Pandas module not available. Please, ensure it is installed and available in your PYTHONPATH.")

        try:
            frame = pd.DataFrame(
                [likelihood_point.param_point for likelihood_point in self.points])

            for stat_type in cfg.stat_types:

                frame['CL{}'.format(stat_type)] = [
                    likelihood_point.combined_exclusion_dict[stat_type] for likelihood_point in self.points]

                if include_dominant_pools:
                    frame['dominant-pool{}'.format(stat_type)] = [
                        likelihood_point.get_dominant_pool(stat_type)
                        for likelihood_point in self.points
                    ]
                    frame['dominant-pool-tag{}'.format(stat_type)] = [
                        likelihood_point.pool_histos_dict[stat_type][likelihood_point.get_dominant_pool(stat_type)]
                        for likelihood_point in self.points
                    ]

                if include_per_pool_cls:
                    for likelihood_point in self.points:
                        for pool in likelihood_point.pool_exclusion_dict[stat_type]:
                            frame['{}{}'.format(pool,stat_type)] = likelihood_point.pool_exclusion_dict[stat_type][pool]

            return frame
        except:
            raise
            
    def export(self, path, include_dominant_pools=True, include_per_pool_cls=False):
        self._build_frame(include_dominant_pools, include_per_pool_cls).to_csv(path, index=False)
    
    def write_summary_dict(self, output_opts):
        """
        Write a brief text summary of a conturDepot to a (returned) dictionary,
        intended for use with yoda stream input.

        :param output_opts: list of requested outputs to put in the summary dict
        """  
        summary_dict = {}
        for stat_type in cfg.stat_types:
            summary_dict[stat_type] = {}

            if len(self.points) < 1:
                continue

            # summary function will just read the first entry in the depot.
            #full_like = self.points[0].combined_exclusion_dict[stat_type]
            full_like = self.points[0].get_full_likelihood(stat_type)
            like_blocks = self.points[0].get_sorted_likelihood_blocks(stat_type)

            if "LLR" in output_opts:
                if (full_like.get_ts_s_b() is not None and full_like.get_ts_b() is not None):
                    summary_dict[stat_type]["LLR"] = full_like.get_ts_s_b() - full_like.get_ts_b()
                else:
                    summary_dict[stat_type]["LLR"] = 0.0

            if "CLs" in output_opts:
                summary_dict[stat_type]["CLs"] = full_like.getCLs()

            if "Pool_LLR" in output_opts:
                summary_dict[stat_type]["Pool_LLR"] = {}
                for block in like_blocks:
                    if ((block.get_ts_s_b(stat_type) is not None)
                    and (block.get_ts_b(stat_type) is not None)):
                        summary_dict[stat_type]["Pool_LLR"][block.pools] = (
                            block.get_ts_s_b(stat_type) - block.get_ts_b(stat_type))
                    else:
                        summary_dict[stat_type]["Pool_LLR"][block.pools] = 0.0

            if "Pool_CLs" in output_opts:
                summary_dict[stat_type]["Pool_CLs"] = {}
                for block in like_blocks:
                    summary_dict[stat_type]["Pool_CLs"][block.pools] = block.getCLs(stat_type)

            if "Pool_tags" in output_opts:
                summary_dict[stat_type]["Pool_tags"] = {}
                for block in like_blocks:
                    summary_dict[stat_type]["Pool_tags"][block.pools] = block.tags

        return summary_dict

       
    @property
    def points(self):
        """
        The master list of :class:`~contur.factories.depot.LikelihoodPoint` instances added to the Depot instance

        **type** ( ``list`` [ :class:`~contur.factories.depot.LikelihoodPoint` ])
        """
        return self._point_list
    
    @property
    def frame(self):
        """
        A ``pandas.DataFrame`` representing the CLs interval for each point in :attr:`points`

        **type** (``pandas.DataFrame``)
        """
        return self._build_frame()

    def __repr__(self):
        return "%s with %s added points" % (self.__class__.__name__, len(self.points))


    
def ts_to_cls(ts_tuple_list):
    """
    calculate the final cls value
    """
    if type(ts_tuple_list) == tuple:
        ts_tuple_list = [ts_tuple_list] #place in list

    log_p_vals = spstat.norm.logsf(np.sqrt(np.array(ts_tuple_list)))
    cls = []

    for ts_index in range(len(log_p_vals)):
        log_pval_b = log_p_vals[ts_index][1]
        log_pval_sb = log_p_vals[ts_index][0]

        try:
            # have stayed with logs for as long as possible for numerical stability
            cls_ts_index = 1 - np.exp(log_pval_sb - log_pval_b)
        except FloatingPointError:
            cls_ts_index = 1

        if (cls_ts_index is not None and cls_ts_index < 0):
            cls_ts_index = 0

        cls.append(cls_ts_index)
    
    return cls  
