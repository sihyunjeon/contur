# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 11.2.0 for Mac OS X x86 (64-bit) (September 11, 2017)
# Date: Fri 1 Dec 2017 14:24:58


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '(ee**2*complex(0,1))/(2.*cw)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-(Ca*ee**2)/(2.*cw)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '(Ca*ee**2)/(2.*cw)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '(complex(0,1)*g1p)/3.',
                 order = {'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(complex(0,1)*g1p)',
                 order = {'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '2*Ca*g1p',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '8*complex(0,1)*g1p**2',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = '8*Ca**2*complex(0,1)*g1p**2',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = 'I1a11',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'I1a12',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = 'I1a13',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = 'I1a21',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = 'I1a22',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'I1a23',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = 'I1a31',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = 'I1a32',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = 'I1a33',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-I2a11',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-I2a12',
                 order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '-I2a13',
                 order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-I2a21',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '-I2a22',
                 order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '-I2a23',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '-I2a31',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-I2a32',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '-I2a33',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = 'I3a11',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = 'I3a12',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = 'I3a13',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = 'I3a21',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = 'I3a22',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = 'I3a23',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = 'I3a31',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = 'I3a32',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = 'I3a33',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-I4a11',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-I4a12',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '-I4a13',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-I4a21',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-I4a22',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '-I4a23',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-I4a31',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '-I4a32',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '-I4a33',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '-2*complex(0,1)*lam1',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '-4*complex(0,1)*lam1',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-6*complex(0,1)*lam1',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '-6*complex(0,1)*lam2',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-(complex(0,1)*lam3)',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '-2*Ca*complex(0,1)*g1p*MZp',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(ee**2*Sa)/(2.*cw)',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '(ee**2*Sa)/(2.*cw)',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '-2*g1p*Sa',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '-8*Ca*complex(0,1)*g1p**2*Sa',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = '2*complex(0,1)*g1p*MZp*Sa',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '8*complex(0,1)*g1p**2*Sa**2',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '2*Ca*complex(0,1)*lam2*Sa - Ca*complex(0,1)*lam3*Sa',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '-2*Ca*complex(0,1)*lam1*Sa + Ca*complex(0,1)*lam3*Sa',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '-(Ca**2*complex(0,1)*lam3) - 2*complex(0,1)*lam1*Sa**2',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '-(Ca**2*complex(0,1)*lam3) - 2*complex(0,1)*lam2*Sa**2',
                 order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '-2*Ca**2*complex(0,1)*lam1 - complex(0,1)*lam3*Sa**2',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-2*Ca**2*complex(0,1)*lam2 - complex(0,1)*lam3*Sa**2',
                 order = {'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '-6*Ca**3*complex(0,1)*lam1*Sa + 3*Ca**3*complex(0,1)*lam3*Sa + 6*Ca*complex(0,1)*lam2*Sa**3 - 3*Ca*complex(0,1)*lam3*Sa**3',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '6*Ca**3*complex(0,1)*lam2*Sa - 3*Ca**3*complex(0,1)*lam3*Sa - 6*Ca*complex(0,1)*lam1*Sa**3 + 3*Ca*complex(0,1)*lam3*Sa**3',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '-6*Ca**4*complex(0,1)*lam2 - 6*Ca**2*complex(0,1)*lam3*Sa**2 - 6*complex(0,1)*lam1*Sa**4',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '-6*Ca**4*complex(0,1)*lam1 - 6*Ca**2*complex(0,1)*lam3*Sa**2 - 6*complex(0,1)*lam2*Sa**4',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(Ca**4*complex(0,1)*lam3) - 6*Ca**2*complex(0,1)*lam1*Sa**2 - 6*Ca**2*complex(0,1)*lam2*Sa**2 + 4*Ca**2*complex(0,1)*lam3*Sa**2 - complex(0,1)*lam3*Sa**4',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '-2*Can1*complex(0,1)*g1p*San1',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = 'Can1**2*complex(0,1)*g1p - complex(0,1)*g1p*San1**2',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-(Can1**2*complex(0,1)*g1p) + complex(0,1)*g1p*San1**2',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-2*Can2*complex(0,1)*g1p*San2',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = 'Can2**2*complex(0,1)*g1p - complex(0,1)*g1p*San2**2',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '-(Can2**2*complex(0,1)*g1p) + complex(0,1)*g1p*San2**2',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '-2*Can3*complex(0,1)*g1p*San3',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = 'Can3**2*complex(0,1)*g1p - complex(0,1)*g1p*San3**2',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '-(Can3**2*complex(0,1)*g1p) + complex(0,1)*g1p*San3**2',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '(Ca**2*ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '(Ca*ee**2*complex(0,1)*Sa)/(2.*sw**2)',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = '(ee**2*complex(0,1)*Sa**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '-(Ca*ee)/(2.*sw)',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '(Can1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(Can2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(Can3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(CKM1x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '(CKM2x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '(CKM3x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '(CKM3x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '(CKM3x3*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(ee**2*complex(0,1))/(2.*sw)',
                  order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '-(Ca*ee**2)/(2.*sw)',
                  order = {'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(Ca*ee**2)/(2.*sw)',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '-(ee*Sa)/(2.*sw)',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(ee**2*Sa)/(2.*sw)',
                  order = {'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '(ee**2*Sa)/(2.*sw)',
                  order = {'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = '-((ee*complex(0,1)*San1)/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-((ee*complex(0,1)*San2)/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-((ee*complex(0,1)*San3)/(sw*cmath.sqrt(2)))',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '-(Ca*cw*ee)/(2.*sw) - (Ca*ee*sw)/(2.*cw)',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(Can1**2*cw*ee*complex(0,1))/(2.*sw) - (Can1**2*ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(Can2**2*cw*ee*complex(0,1))/(2.*sw) - (Can2**2*ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(Can3**2*cw*ee*complex(0,1))/(2.*sw) - (Can3**2*ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '(cw*ee**2*complex(0,1))/sw - (ee**2*complex(0,1)*sw)/cw',
                  order = {'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '-(cw*ee*Sa)/(2.*sw) - (ee*Sa*sw)/(2.*cw)',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '(Can1*cw*ee*complex(0,1)*San1)/(2.*sw) + (Can1*ee*complex(0,1)*San1*sw)/(2.*cw)',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-(cw*ee*complex(0,1)*San1**2)/(2.*sw) - (ee*complex(0,1)*San1**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(Can2*cw*ee*complex(0,1)*San2)/(2.*sw) + (Can2*ee*complex(0,1)*San2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-(cw*ee*complex(0,1)*San2**2)/(2.*sw) - (ee*complex(0,1)*San2**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '(Can3*cw*ee*complex(0,1)*San3)/(2.*sw) + (Can3*ee*complex(0,1)*San3*sw)/(2.*cw)',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '-(cw*ee*complex(0,1)*San3**2)/(2.*sw) - (ee*complex(0,1)*San3**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = 'Ca**2*ee**2*complex(0,1) + (Ca**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (Ca**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = 'Ca*ee**2*complex(0,1)*Sa + (Ca*cw**2*ee**2*complex(0,1)*Sa)/(2.*sw**2) + (Ca*ee**2*complex(0,1)*Sa*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = 'ee**2*complex(0,1)*Sa**2 + (cw**2*ee**2*complex(0,1)*Sa**2)/(2.*sw**2) + (ee**2*complex(0,1)*Sa**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '(ee**2*vev)/(2.*cw)',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '(ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(Ca*ee**2*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '(Ca*ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(ee**2*complex(0,1)*Sa*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '(ee**2*complex(0,1)*Sa*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '(ee**2*vev)/(2.*sw)',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-(Ca*ee**2*complex(0,1)*vev)/2. - (Ca*cw**2*ee**2*complex(0,1)*vev)/(4.*sw**2) - (Ca*ee**2*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = 'Ca*ee**2*complex(0,1)*vev + (Ca*cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (Ca*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(ee**2*complex(0,1)*Sa*vev)/2. - (cw**2*ee**2*complex(0,1)*Sa*vev)/(4.*sw**2) - (ee**2*complex(0,1)*Sa*sw**2*vev)/(4.*cw**2)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = 'ee**2*complex(0,1)*Sa*vev + (cw**2*ee**2*complex(0,1)*Sa*vev)/(2.*sw**2) + (ee**2*complex(0,1)*Sa*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '8*Ca*complex(0,1)*g1p**2*xev',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '-8*complex(0,1)*g1p**2*Sa*xev',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(complex(0,1)*lam3*Sa*vev) - 2*Ca*complex(0,1)*lam2*xev',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '-2*complex(0,1)*lam1*Sa*vev - Ca*complex(0,1)*lam3*xev',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-(Ca*complex(0,1)*lam3*vev) + 2*complex(0,1)*lam2*Sa*xev',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-2*Ca*complex(0,1)*lam1*vev + complex(0,1)*lam3*Sa*xev',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '-6*Ca**2*complex(0,1)*lam1*Sa*vev + 2*Ca**2*complex(0,1)*lam3*Sa*vev - complex(0,1)*lam3*Sa**3*vev - Ca**3*complex(0,1)*lam3*xev - 6*Ca*complex(0,1)*lam2*Sa**2*xev + 2*Ca*complex(0,1)*lam3*Sa**2*xev',
                  order = {'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '-3*Ca**2*complex(0,1)*lam3*Sa*vev - 6*complex(0,1)*lam1*Sa**3*vev - 6*Ca**3*complex(0,1)*lam2*xev - 3*Ca*complex(0,1)*lam3*Sa**2*xev',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '-6*Ca**3*complex(0,1)*lam1*vev - 3*Ca*complex(0,1)*lam3*Sa**2*vev + 3*Ca**2*complex(0,1)*lam3*Sa*xev + 6*complex(0,1)*lam2*Sa**3*xev',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '-(Ca**3*complex(0,1)*lam3*vev) - 6*Ca*complex(0,1)*lam1*Sa**2*vev + 2*Ca*complex(0,1)*lam3*Sa**2*vev + 6*Ca**2*complex(0,1)*lam2*Sa*xev - 2*Ca**2*complex(0,1)*lam3*Sa*xev + complex(0,1)*lam3*Sa**3*xev',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(yb/cmath.sqrt(2))',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '-((Ca*complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-((complex(0,1)*Sa*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = 'yc/cmath.sqrt(2)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-((Ca*complex(0,1)*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-((complex(0,1)*Sa*yc)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '-(ydo/cmath.sqrt(2))',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '-((Ca*complex(0,1)*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '-((complex(0,1)*Sa*ydo)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(ye/cmath.sqrt(2))',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-((Ca*complex(0,1)*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-(Can1*ye)',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = 'Can1*ye',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '-((complex(0,1)*Sa*ye)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-(San1*ye)',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = 'San1*ye',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '-(ym/cmath.sqrt(2))',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '-((Ca*complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '-(Can2*ym)',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = 'Can2*ym',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-((complex(0,1)*Sa*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '-(San2*ym)',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = 'San2*ym',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '-(Can1*ynd1)',
                  order = {'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = 'Can1*ynd1',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(San1*ynd1)',
                  order = {'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = 'San1*ynd1',
                  order = {'QED':1})

GC_197 = Coupling(name = 'GC_197',
                  value = 'Can1*San1*ynd1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-((Can1**2*ynd1)/cmath.sqrt(2)) + (San1**2*ynd1)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '-(Can2*ynd2)',
                  order = {'QED':1})

GC_200 = Coupling(name = 'GC_200',
                  value = 'Can2*ynd2',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(San2*ynd2)',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = 'San2*ynd2',
                  order = {'QED':1})

GC_203 = Coupling(name = 'GC_203',
                  value = 'Can2*San2*ynd2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '-((Can2**2*ynd2)/cmath.sqrt(2)) + (San2**2*ynd2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '-(Can3*ynd3)',
                  order = {'QED':1})

GC_206 = Coupling(name = 'GC_206',
                  value = 'Can3*ynd3',
                  order = {'QED':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-(San3*ynd3)',
                  order = {'QED':1})

GC_208 = Coupling(name = 'GC_208',
                  value = 'San3*ynd3',
                  order = {'QED':1})

GC_209 = Coupling(name = 'GC_209',
                  value = 'Can3*San3*ynd3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '-((Can3**2*ynd3)/cmath.sqrt(2)) + (San3**2*ynd3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_211 = Coupling(name = 'GC_211',
                  value = 'Can1**2*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_212 = Coupling(name = 'GC_212',
                  value = 'Can1*San1*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '-(San1**2*ynm1*cmath.sqrt(2))',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = 'Can1*complex(0,1)*Sa*San1*ynd1*cmath.sqrt(2) - Ca*Can1**2*complex(0,1)*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = 'Ca*Can1*complex(0,1)*San1*ynd1*cmath.sqrt(2) + Can1**2*complex(0,1)*Sa*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '-((Can1**2*complex(0,1)*Sa*ynd1)/cmath.sqrt(2)) + (complex(0,1)*Sa*San1**2*ynd1)/cmath.sqrt(2) - Ca*Can1*complex(0,1)*San1*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '-((Ca*Can1**2*complex(0,1)*ynd1)/cmath.sqrt(2)) + (Ca*complex(0,1)*San1**2*ynd1)/cmath.sqrt(2) + Can1*complex(0,1)*Sa*San1*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = 'Can1*complex(0,1)*Sa*San1*ynd1*cmath.sqrt(2) + Ca*complex(0,1)*San1**2*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = 'Ca*Can1*complex(0,1)*San1*ynd1*cmath.sqrt(2) - complex(0,1)*Sa*San1**2*ynm1*cmath.sqrt(2)',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = 'Can2**2*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_221 = Coupling(name = 'GC_221',
                  value = 'Can2*San2*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '-(San2**2*ynm2*cmath.sqrt(2))',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = 'Can2*complex(0,1)*Sa*San2*ynd2*cmath.sqrt(2) - Ca*Can2**2*complex(0,1)*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = 'Ca*Can2*complex(0,1)*San2*ynd2*cmath.sqrt(2) + Can2**2*complex(0,1)*Sa*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '-((Can2**2*complex(0,1)*Sa*ynd2)/cmath.sqrt(2)) + (complex(0,1)*Sa*San2**2*ynd2)/cmath.sqrt(2) - Ca*Can2*complex(0,1)*San2*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '-((Ca*Can2**2*complex(0,1)*ynd2)/cmath.sqrt(2)) + (Ca*complex(0,1)*San2**2*ynd2)/cmath.sqrt(2) + Can2*complex(0,1)*Sa*San2*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = 'Can2*complex(0,1)*Sa*San2*ynd2*cmath.sqrt(2) + Ca*complex(0,1)*San2**2*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = 'Ca*Can2*complex(0,1)*San2*ynd2*cmath.sqrt(2) - complex(0,1)*Sa*San2**2*ynm2*cmath.sqrt(2)',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = 'Can3**2*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = 'Can3*San3*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '-(San3**2*ynm3*cmath.sqrt(2))',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = 'Can3*complex(0,1)*Sa*San3*ynd3*cmath.sqrt(2) - Ca*Can3**2*complex(0,1)*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = 'Ca*Can3*complex(0,1)*San3*ynd3*cmath.sqrt(2) + Can3**2*complex(0,1)*Sa*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '-((Can3**2*complex(0,1)*Sa*ynd3)/cmath.sqrt(2)) + (complex(0,1)*Sa*San3**2*ynd3)/cmath.sqrt(2) - Ca*Can3*complex(0,1)*San3*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-((Ca*Can3**2*complex(0,1)*ynd3)/cmath.sqrt(2)) + (Ca*complex(0,1)*San3**2*ynd3)/cmath.sqrt(2) + Can3*complex(0,1)*Sa*San3*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = 'Can3*complex(0,1)*Sa*San3*ynd3*cmath.sqrt(2) + Ca*complex(0,1)*San3**2*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = 'Ca*Can3*complex(0,1)*San3*ynd3*cmath.sqrt(2) - complex(0,1)*Sa*San3**2*ynm3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '-(ys/cmath.sqrt(2))',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '-((Ca*complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '-((complex(0,1)*Sa*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_241 = Coupling(name = 'GC_241',
                  value = 'yt/cmath.sqrt(2)',
                  order = {'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '-((Ca*complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '-((complex(0,1)*Sa*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '-(ytau/cmath.sqrt(2))',
                  order = {'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '-((Ca*complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '-(Can3*ytau)',
                  order = {'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = 'Can3*ytau',
                  order = {'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '-((complex(0,1)*Sa*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '-(San3*ytau)',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = 'San3*ytau',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = 'yup/cmath.sqrt(2)',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '-((Ca*complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '-((complex(0,1)*Sa*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '(ee*complex(0,1)*complexconjugate(CKM3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

