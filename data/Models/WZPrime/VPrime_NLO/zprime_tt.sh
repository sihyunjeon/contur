import model <RunInfo/VPrime_NLO>
generate p p > zp > t t~ [QCD]
output mgevents
launch
shower=PYTHIA8
set mzp {mzp}
set kl {kl}
set wzp Auto
