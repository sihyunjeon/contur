# Additional or Modified Rivet info

Staging area for new or modified rivet routines (and REF data, metadata) before they have moved into
a rivet release.

(this version for running with rivet 3.1.8)

If you have a new rivet routine that you want to use locally with Contur, place the .cc, .plot, .info and .yoda(.gz) files in this directory, and it will be compiled when you next execute `make` for Contur. The same applies for any modified versions of existing Rivet files - they will override any files in your existing Rivet release. If you have the .cc file here, you will see a warning about a dulicated Rivet analysis - this is expected and harmless.

For a new Rivet routine, you will also need to add it to the Contur [analysis database](../DB/analysis.sql). See [here](../DB/README.md) for some help on that.




   
