# Running a batch job to Generate Heatmaps

## Setting up the batch job
The command to create multiple YODA files in a batch job is (surprisingly) `contur-batch`. This will send different jobs to the batch system, each job corresponding to a point in your parameter space. Furthermore, 

In the same run-area from [your first run](firstrun.md), run a test scan over the parameter space defined in 'param_file.dat' without submitting it
to a batch farm.  (The `-s` flag ensures no jobs will be submitted.)

         $ contur-batch -n 1000 --seed 101 -s

  or if you have a version of Herwig without the rivet interface installed,use the pipe option:

         $ contur-batch -n 1000 --seed 101 -s -P

Type `contur-batch --help` to see all the available options.

This will produce a directory called 'myscan00' (or some higher integer if myscan00 already existed) containing one directory for each selection beam energy (just 13TeV pp by default), containing however many runpoint directories are indicated by the ranges in your param_file.dat. Have a look at the shell scripts (`runpoint_xxxx.sh`) which have been generated and the `herwig.in` files to check all is as you expected. 

You can manually submit some of the `runpoint_xxxx.sh` files as a test:

         $ cd myscan00/13TeV/0000
         $ source runpoint_0000.sh

or run Herwig local as above using the generated `herwig.in` files.

- If you do not encounter any problems running on a single point, you are ready to run batch jobs. Remove the myscan00 directory tree you just created, and run the
  batch submit command again, now without the `-s` flag and specifying the queue
  on your batch farm. For example:

         $ contur-batch -n 1000 --seed 101 -Q medium

  or
  
         $ contur-batch -n 1000 --seed 101 -P -Q medium


  (Note that we assume `qsub` is available on your system here and has a queue called `medium`.
  Slurm and condor batch systems are also supported, and of course you can change the queue name.
  If you have a different submission system you'll need to
  look into `contur/scan/batch_submit.py` and work out how to change the appropriate submission
  commands.)

- A successful run will produce a directory called 'myscan00' as before. You need to wait for the farm
  to finish the jobs before continuing. On PBS, you can check the progress using the 'qstat' command.

- When the batch job is complete there should, in every run point directory, be
  files `herwig-runpoint_xxx.yoda`.

## Analyse results with contur. 

Now it's time to do some statistics with your set of YODA files and set exlucions on the BSM parameter space!

For this, we use the `contur` executable where we point the grid flag `-g` to the scan folder: 

        $ contur -g myscan00/

The output will be a database (.db) file in the ANALYSIS folder.

For various options, see:

        $ contur --help

You may find the `--mpl` flag useful when analysing the runpoints individually. Some more info on this is given [below](#analysing-the-individual-runpoints).

## Plot a heatmap

The `contur_run.db` database file contains all the information to make 2D exclusion plots. The commands are simple:

        $ cd ANALYSIS/
        $ contur-plot --help
        $ contur-plot contur_run.db mY1 mXd  -T "My First Heatmap"

## Analysing the individual runpoints

After looking at the 2D exclusion plot, you may want to focus on a single point in the two-dimensional parameter space. For example to understand which analysis contributes how much to the exclusion that given point. 

This is a straightforward exercise, using a set of plotting scripts that are already generated and stored in `ANALYSIS/plots/`. Have a browse through this folder and you will find that each histogram for all the analyses have an associated Python script. By executing these Python scripts you generate plots showing the differential distributions with the data, SM prediction and SM+BSM prediction.

To do this, go back to your run area and show the level of exclusion for every histograms from each analysis as follows:

        $ cd ..
        $ contur-rivetplots -i ANALYSIS --runpoint 13TeV/0010 -p
        INFO - Contur version 2.5.0
        INFO - See https://hepcedar.gitlab.io/contur-webpage/
        Writing log to contur.log
        INFO - Read DB file ANALYSIS/contur_run.db
        INFO - Looking for plot scripts in ANALYSIS/plots/13TeV/0010
        ATLAS_13_SSLLMET
        - ATLAS_2019_I1738841
        -- d01-x01-y01 : 0.01(DATABG) 0.01(SMBG) 0.01(EXP) 

        ATLAS_13_METJET
        - ATLAS_2017_I1609448
        -- d02-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        -- d01-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        -- d04-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        -- d03-x01-y01 : 0.00(DATABG) 0.00(SMBG) 0.01(EXP) 
        ...
       
You specify the point on the grid with the `--gridpoint` command-line argument. The `-p` (`--print`) flag ensures the info per histogram is only printed to the terminal and no plots are generated yet. As you can see in the example above, there might be many histograms that do not contribute (much) to the actual exclusion. It may not be necessary to generate plots for all of these, therefore two options allow you to filter the output by the minimum level of CLs and the name of the analysis, for example:

        $ contur-rivetplots -i ANALYSIS --runpoint 13TeV/0010 -p --cls 0.9 --ana-match ATLAS_13
        INFO - Contur version 2.5.0
        INFO - See https://hepcedar.gitlab.io/contur-webpage/
        Writing log to contur.log
        INFO - Read DB file ANALYSIS/contur_run.db
        INFO - Looking for plot scripts in ANALYSIS/plots/13TeV/0010
        ATLAS_13_TTHAD
        - ATLAS_2022_I2077575
        -- d32-x01-y01 : 0.99(DATABG) 1.00(SMBG) 0.99(EXP) 
        -- d05-x01-y01 : 1.00(DATABG) 1.00(SMBG) 1.00(EXP) 
        -- d50-x01-y01 : 0.94(DATABG) 0.99(SMBG) 0.94(EXP) 
        -- d08-x01-y01 : 0.68(DATABG) 1.00(SMBG) 0.68(EXP) 
        -- d41-x01-y01 : 1.00(DATABG) 1.00(SMBG) 1.00(EXP) 
        -- d14-x01-y01 : 0.72(DATABG) 0.97(SMBG) 0.72(EXP) 
        -- d58-x01-y01 : 0.72(DATABG) 0.99(SMBG) 0.72(EXP) 
        -- d44-x01-y01 : 0.79(DATABG) 0.94(SMBG) 0.79(EXP) 
        ...

This command will then only display the histograms using ATLAS 13TeV data with a minimum exclusion of 0.9 (90%). If you are happy with the filter settings, you can remove the `-p` flag and hit enter. This plots the data, SM prediction and SM+BSM prediction for the histograms that pass your filter. You can browse these using the generated HTML booklet:

        $ open ANALYSIS/plots/13TeV/0010/index.html

## Other functionality

`contur-gridtool` provides some utilities for compressing or merging grids, recovering failed grid points, and locating the files corresponding to a particular parameter point.

