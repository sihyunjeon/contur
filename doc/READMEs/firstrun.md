# First Contur run

## Using exisiting YODA files
**You are now ready to run contur!** The first step is to work with a single YODA file. The commands are simple:

        $ contur myrivetresults.yoda

- Run `contur --help` to see more options. 

- Normally, this YODA file contains histograms corresponding to a set of Rivet analyses. The histograms are filled with events from your favourite BSM theory. By running `contur` on this YODA file, you check the compatibility of the BSM theory with the results from experimental data. In this first run, we focus on a *single* point of parameter space. Later on, we will see how to [generate 2D exclusion plots](exclusions2D.md).

- There are sample yoda files to try this on in the test area, under `tests/sources/myscan00`. Running `make check` also runs contur on these, however as a user you most likely won't have write permission to the installation directory, in which case this will give errors. Running `contur -g $CONTUR_DATA_PATH/tests/sources/myscan00` from your own working area should complete, however. 

- This will output an ANALYSIS folder and a log file, and print some other information to the screen.

- You can then generate a 'contur-plots' directory containing histograms and an index.html
  file to view them all: whilst in the directory containing this yoda file, run:

        $ contur-mkhtml                     # to generate the contur-plots directory
        $ open contur-plots/index.html      # to browse the HTML booklet with your plots

## Generating YODA files from HepMC outputs
If you have produced a HepMC event file from somewhere and want to run Contur on that, you need to run Rivet on your events.

        $ rivet -a $ANALYSIS-LIST myfile.hepmc

where $ANALYSIS-LIST is a comma-separated list of the analyses you want to run. Contur defines some convenient environment
variables with the list of relevant analyses for 7, 8 and 13 TeV LHC running, $CONTUR_RA7TeV, $CONTU_RA8TeV and $CONTUR_RA13TeV.
Rivet will produce a .yoda file which you can then use as in the previous steps. For other rivet command line options, see

        $ rivet --help

as usual.

## Generating BSM events 

To do this you of course need a working event generator. Contur current provides some tools to work with Herwig, Pythia and Madgraph, and there is some documentation for each of these on the wiki. This example with take you through using Herwig, though it will flag up where things would change for other generators. If you are not familiar with Herwig, you can check out these [Herwig tips and tricks](https://gitlab.com/hepcedar/contur/-/wikis/Herwig-tips-and-tricks) and/or the official [Herwig7 tutorials](https://herwig.hepforge.org/tutorials/index.html).

Create a run area seperate from the repository where you installed Contur. This run area is where you will run everything from now on.

        $ mkdir run-area
        $ cd run-area
        $ mkdir RunInfo

### Choose a model

You should copy a UFO model directory from somewhere. Many of those previously used in contur are in '$CONTUR_DATA_PATH/data/Models', for example DMsimp_s_spin1 is a widely used simplified Dark Matter model, with some Contur results [here](https://hepcedar.gitlab.io/contur-webpage/results/DMsimp_s_spin1/index.html).

        $ cd RunInfo
        $ cp -r $CONTUR_DATA_PATH/data/Models/DM/DMsimp_s_spin1 .

- A template `param_file.dat` will be provided for each model. This is generator-independent and lists the parameters and scan ranges. Here is an example : [`data/Models/DM/DMsimp_s_spin1/param_file.dat`](data/Models/DM/DMsimp_s_spin1/param_file.dat). There are many different ways to generate your scan ranges such as linear, logarithmic or relative to another parameters. Click [here](param_files.md) for an extensive overview.

- Template Herwig `herwig.in` files are provided in the many of the model directories (obviously these are generator-dependent, 
   and we'll provide examples for other generators when we can). The  `herwig.in` file specifies parameters in curly brackets, e.g. 
   `.{name}` for future use with the batch system. 

- Build the UFO model using Herwig's 'ufo2herwig' command.

        $ ufo2herwig DMsimp_s_spin1
        $ make

### Herwig and Rivet combined run on a Single single set of Rivet Analyses

This section is specific to a single run of Herwig with one of these models, a recommended first step.

- Copy the template `herwig.in` file from inside the model to the top level of your run area.

        $ cd run-area
        $ cp RunInfo/DMsimp_s_spin1/herwig.in .
        $ cp RunInfo/DMsimp_s_spin1/param_file.dat .

- Build the full herwig input file

        $ contur-batch --single 

  This will create a directory called `myscan00/13TeV/0000` which will contain a `herwig.in` file with the full instructions
  for a run, and with BSM model variables substituted from `param_file.dat`. There will be some other files which would be used
  if we were generating a scan, but which you can ignore for now.

- Build the Herwig run card (herwig.run).

        $ cd myscan00/13TeV/0000
        $ Herwig read herwig.in -I ../../../RunInfo -L ../../../RunInfo

- Run the Herwig run card, specifying the number of events to generate. This
  can take a while so, as a first test, running around 200 events is fine.

        $ Herwig run herwig.run  -N 200

- This will produce the file herwig.yoda containing the results of the Herwig run. You can run on this as described above (running on a single Yoda file).

Note that if you are running a generator independently of Rivet, you can use a pipe for this, so the (sometimes large) file never has to be resident on disk. For example:

        $ mkfifo fifo.hepmc
        $ run-pythia -n 200000 -e 8000 -c Top:all=on -o fifo.hepmc &
        $ rivet fifo.hepmc -a $RA8TeV

If you have a version of Herwig built without the rivet interface, you will also need to run via this pipe. The appropriate `herwig.in` file can be built using the `-P` option for `contur-batch`.