# Installation

Contur and Rivet are generator-independent, and so as long as you have an event generator producing
HepMC files, you can run those events through Rivet and then run Contur on the resulting yoda file. However, Contur
also provides tools to scan over model parameters generating events, which of course requires an event generator to 
be installed. These instructions will guide you through first of all setting up the minimal Contur installation, to run on an existing yoda file, and then take you through running Rivet on an existing file of HepMC events, some ways of generating those 
HepMC files with a BSM model, and finally running a parameter scan using a BSM model and an event generator, then running Contur on that to get a senstitvity heatmap.

## Dependencies

You will need a working installation of [Rivet](https://rivet.hepforge.org/) and [Yoda](https://yoda.hepforge.org/).
You can find installation instructions on their web pages, [Rivet](https://rivet.hepforge.org) and 
[Yoda](https://yoda.hepforge.org), or obtain them from their gitlab repositories in the [Cedar group](https://gitlab.com/hepcedar). 

You need to make sure your environment is set up appropriately for the required version Rivet etc. If these are in $CEDARINSTDIR, for example:

        $ source $CEDARINSTDIR/rivetenv.sh
        $ source $CEDARINSTDIR/yodaenv.sh

This needs be done every time you start a shell, so you might want to add it to your .bashrc file or similar.

You will need a Python 3 development environment.

To generate your own events and parameter scans (later steps), you will need to install at least one event generator, such as
[Herwig](https://herwig.hepforge.org/), MadGraph or Pythia. The examples in these tutorial pages use Herwig and MadGraph.

## Installing Contur into your Python virtual environment
*If you are developing `contur`, now go straight to the [`Developer Instructions`](./developers.md), and rejoin these instructions at "You are now ready..."*

You can simply use the Python package installer to install Contur.

        $ pip install contur

### Set up the contur environment

- Set some environment variables so that you can use the Contur executables from anywhere. You will need to find the path of your virtual Python environment (for example with `which contur`) and:

        $ source your_virtual_python_path/bin/conturenv.sh

  Since this needs to be done every time you start a shell, we suggest adding it to the script where you source `rivetenv.sh` and `yodaenv.sh`

- Set up your own user path:   After sourcing this bash script, you will find that the environment variables ``$CONTUR_DATA_PATH`` and ``$CONTUR_USER_DIR`` have been set. By default ``$CONTUR_USER_DIR`` will be set to ``~/contur_users``, but you can change it to your prefered location. Note that this will be the place in which all the generated files needed by contur are installed in thr next step.

- Now run `make`, which is under your ``$CONTUR_DATA_PATH``:

        $ cd $CONTUR_DATA_PATH
        $ make

  and follow the instructions in the `make` output. You need only do this once. It will build any modified rivet analyses, build the database of static analysis information, containing some analysis steering files for the generators under ``$CONTUR_USER_DIR`` (e.g. `*.ana` for Herwig) and create a script to define some environment variables which can be used to run rivet on a HepMC file. 

You can finally run `make check` to ensure everything was installed succesfully and the paths are set properly. Now you are ready to do your first [Contur run](firstrun.md)!