# Running machine-learning assisted parameter scanning (the CONTUR ORACLE)

The CONTUR ORACLE is the name given to the machine-learning-assisted parameter scanning functionality described in [https://arxiv.org/abs/2202.05882](https://arxiv.org/abs/2202.05882).
The basic idea is that a Random-Forest classifier is used on sampled points to predict the exclusion status of nearby points, thereby reducing the total number of points needed when studying large (multi-dimensional) grids.
Additional Python libraries which are needed are: `sklearn` and `click`.

The setup of a scan if very similar to that described above in `Running a Batch Job to Generate Heatmap`.
Once the UFO file has been copied and compiled, one should initialise the ORACLE using

        contur-oracle init

Which will create a dummy `oracle.config.yaml` file and the appropriate directories for each iteration.
First, choose the parameters of the model which you wish to scan using the ORACLE.
Those should be specified in `oracle.config.yaml`  `params` section with a `range` vector specifying the extremal values of the parameter, as well as a `resolution` parameter which dictates the minimum separation between points.
For example:

        params:
          gVq1:
            range:
            - 0.09
            - 0.99
            resolution: 0.1
          gVq2:
            range:
            - 0.09
            - 0.99
            resolution: 0.1
          gVq3:
            range:
            - 0.09
            - 0.99
            resolution: 0.1
          mXd:
            range:
            - 10.0
            - 5010.0
            resolution: 250
          mY1:
            range:
            - 10.0
            - 10010.0
            resolution: 500

One should also specify the number of points to sample in the initial and subsequent iterations, and the stopping conditions (in precision, recall and entropy) after which the ORACLE is deemed to have converged. For example:

          initial_points: 500
          iteration_points: 300
          precision_goal: 0.90
          recall_goal: 0.90
          entropy_goal: 0.2


In the `param _file.dat`, the parameters which are listed in the `oracle.config.yaml` should be listed in `DATAFRAME` mode and with the dummy `DATAFRAME_LOCATION` being used as the `name` parameter. For example:
          
          [Parameters]
          [[mXd]]
          mode = DATAFRAME
          name = DATAFRAME_LOCATION
          [[mY1]]
          mode = DATAFRAME
          name = DATAFRAME_LOCATION
          [[gVq1]]
          mode = DATAFRAME
          name = DATAFRAME_LOCATION
          [[gVq2]]
          mode = DATAFRAME
          name = DATAFRAME_LOCATION
          [[gVq3]]
          mode = DATAFRAME
          name = DATAFRAME_LOCATION
          [[gVl]]
          mode = CONST
          value = 0.0
          [[gVXd]]
          mode = CONST
          value = 1.0
          [[gAXd]]
          mode = CONST
          value = 0.0
          [[gAq]]
          mode = CONST
          value = 0.0

For expert users, other hyper-parameters such as the test/train split, the number of trees, and the CL of the contours, can be modified in `contur/oracle/hyperparams.py`.

Once the config file is set up, the ORACLE can be set running using:
         
         contur-oracle start

This will randomly pick a subset of size `initial_points` of the grid, and prepare a CONTUR run for those points.
The user will be prompted as to what to do next with a printout on the screen:



          1. Contur batch to generate the events:
          contur-batch -p /path/to/your/param_files/param_file_1.dat -o contur_batch_output_1 -Q medium --seed 101 -n 30000 -b 13TeV
          
          2. Contur to analyse the points
          contur -g contur_batch_output_1 -o contur_analysis_1
          
          3. Contur export to extract the corresponding CSV with the results
          contur-export -i contur_analysis_1/contur.map -o output_cls/run-output-1.csv
          
          4. Contur oracle to process the results, train the classifier, and determine if more points need to be generated
          contur-oracle start
 
In 1., the user is prompted to submit the jobs to the HPC farm. The user should manually edit the queue name, beams, etc to match their specifications. This part has to be done by the user for security reasons.
The user should then wait until their jobs from 1. have finished (usually a few hours depending on the number of points and the number of nodes on your batch farm), and then can merge them with the command listed in 2.
Once the merge is completed (usually 10 min to an hour or so), the results are exported to a CSV in 3. (a few seconds).

Finally, one can start the next iteration of the ORACLE with 4. : `contur-oracle start`, which will take the points from this iteration and previous ones, separate the sampled points into test/train datasets.
The points in the training dataset are used to train the Random Forest classifier. This classifier is then applied to the testing dataset, and the classifier predictions are compared to the true exclusion status of these points, to obtain the performance metrics of recall, precision and entropy.
These are printed to screen, eg.

          contur-oracle 2021-12-15 16:30:35: it 1: Recall: [0.89/0.95], Precision: [0.75/0.95], Entropy: [0.37/0.20], total points: 342, testing: 114

If the metrics do not yet meet the stopping conditions specified in the configuration file, the next iteration is prepared, sampling the points with the worst entropy values.
The user is then prompted to repeat steps 1.-4. until the performance metrics are met.

All the prompts and performance metrics are printed to screen are also logged in `oracle.log` for future reference.
The classifiers for each iteration are stored in the `classifiers` directory. These are pickle files which can be used as follows to extract predictions:


        import pickle
        classifierFeatures = ["gVq1","gVq2","gVq3","mXd", "mY1"] # as specified in your config file
        file_1 =  open(f"classifiers/classifier-1.pkl", "rb")
        rf_1 = pickle.load(file_1)
        
        grid = ... # array of array of values for the features (ie an arbitrary number of sets of parameter values can be predicted at once)
        pred =  rf.predict(grid) 
        entropy =  entr(rf.predict_proba(grid)).sum(axis=1) / np.log(3)

 `pred` will return a list of 0, 1 or 2, for each row of the `grid`, where 0 is not excluded, 1 is excluded at 68% CL and 2 is excluded at 68-95% CL.
 The entropy can be calculated as shown.