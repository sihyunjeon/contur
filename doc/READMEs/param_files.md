There are different ways to scan over the parameters. All the options are listed in the text block below.
Most likely the options 1-5 give more than enough flexibility for your studies with Contur.

        $ # 1 linear scale:
        $ [[x]]
        $ mode = LIN
        $ start = 0.1
        $ stop = 2.5
        $ number = 40

        $ # 2 logarithmic scale
        $ [[y]]
        $ mode = LOG 
        $ start = 1.0
        $ stop = 200.
        $ number = 10

        $ # 3 define a constant
        $ [[z]] 
        $ mode = CONST 
        $ value = 2112

        $ # 4 relative to other parameters
        $ [[function]]
        $ mode = REL 
        $ form = 15 * {x}
        
        $ # 5 impose a condition parameters should satisfy
        $ [[condition1]]
        $ mode = CONDITION
        $ form = {y} > 50

        $ # 6 Pickled object containing the parameter points
        $ [[dataframe]]
        $ mode = DATAFRAME
        $ name = my_parameters.pickle

        $ # 7 use a single SLHA as the starting point for a scan. Must then provide parameter names to step over 
        $ # (see below).
        $ [[slha_file]]
        $ mode = SINGLE
        $ name = SLHA_file.dat

        $ #  8 using a single SLHA file as the starting point for a scan, specify SLHA parameters to modify/step over.
        $ # This example picks out the mass of particle 1000022.
        $ [[M1000022]]
        $ block = MASS
        $ mode = LIN
        $ start = 10
        $ stop = 220
        $ number = 14

        $ # 9 use a single SLHA as the starting point for a scan, scale all values in the block specified below 
        $ # by a factor which can be stepped over
        $ [[slha_file]]
        $ mode = SCALED
        $ name = SLHA_file.dat

        $ # 10 specify SLHA block to scale, and the factors to scale by.
        $ [[MASS]] 	   
        $ mode = LIN
        $ start = 10
        $ stop = 0.5
        $ number = 1.5

        $ # 11 link to directory containing a colleaction of SLHA files to run over.
        $ [[dir]]
        $ mode = DIR
        $ name = "/abs/path/to/directory"