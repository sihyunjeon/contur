contur.plot package
===================

Submodules
----------

contur.plot.color\_config module
--------------------------------

.. automodule:: contur.plot.color_config
   :members:
   :undoc-members:
   :show-inheritance:

contur.plot.contur\_plot module
-------------------------------

.. automodule:: contur.plot.contur_plot
   :members:
   :undoc-members:
   :show-inheritance:

contur.plot.label\_maker module
-------------------------------

.. automodule:: contur.plot.label_maker
   :members:
   :undoc-members:
   :show-inheritance:

contur.plot.axis\_labels module
-------------------------------

.. automodule:: contur.plot.axis_labels
   :members:
   :undoc-members:
   :show-inheritance:

contur.plot.html\_utils module
------------------------------

.. automodule:: contur.plot.html_utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: contur.plot
   :members:
   :undoc-members:
   :show-inheritance:
