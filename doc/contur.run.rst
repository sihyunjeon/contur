contur.run package
==================

Top level module for running contur. Submodules accessed directly from executable scripts.


Submodules
----------

contur.run.run\_analysis module
-------------------------------

.. automodule:: contur.run.run_analysis
   :members:
   :undoc-members:
   :show-inheritance:

contur.run.run\_batch\_submit module
------------------------------------

.. automodule:: contur.run.run_batch_submit
   :members:
   :undoc-members:
   :show-inheritance:

contur.run.run\_grid\_tools module
----------------------------------

.. automodule:: contur.run.run_grid_tools
   :members:
   :undoc-members:
   :show-inheritance:

contur.run.run\_plot module
----------------------------------

.. automodule:: contur.run.run_plot
   :members:
   :undoc-members:
   :show-inheritance:
      
contur.run.run\_init module
------------------------------

.. automodule:: contur.run.run_init
   :members:
   :undoc-members:
   :show-inheritance:
      
contur.run.run\_mkthy module
------------------------------

.. automodule:: contur.run.run_mkthy
   :members:
   :undoc-members:
   :show-inheritance:
      
contur.run.run\_mkbib module
------------------------------

.. automodule:: contur.run.run_mkbib
   :members:
   :undoc-members:
   :show-inheritance:
      
contur.run.run\_smtest module
------------------------------

.. automodule:: contur.run.run_smtest
   :members:
   :undoc-members:
   :show-inheritance:
      

contur.run.run\_extract\_xs_\bf module
--------------------------------------

.. automodule:: contur.run.run_extract_xs_bf
   :members:
   :undoc-members:
   :show-inheritance:
      
contur.run.arg\_utils module
----------------------------

.. automodule:: contur.run.arg_utils
   :members:
   :undoc-members:
   :show-inheritance:
      
Module contents
---------------

.. automodule:: contur.run
   :members:
   :undoc-members:
   :show-inheritance:
