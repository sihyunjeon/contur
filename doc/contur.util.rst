contur.util package
===================

Submodules
----------

contur.util.file\_readers module
--------------------------------

.. automodule:: contur.util.file_readers
   :members:
   :undoc-members:
   :show-inheritance:

contur.util.utils module
------------------------

.. automodule:: contur.util.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: contur.util
   :members:
   :undoc-members:
   :show-inheritance:
