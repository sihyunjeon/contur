.. contur documentation master file, created by
   sphinx-quickstart on Tue Oct  6 15:24:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

In-code documentation for Contur
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   contur


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
