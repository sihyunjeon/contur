Docker build configurations for a `contur`=Contur+Rivet and a `contur-herwig`=Contur+Rivet+Herwig image.

The Dockerfiles take arguments, and the shell scripts loop over the argument combinations you want

The build scripts are sensitive to some environment variables, so e.g. `./build.sh` just does the build & tag loops, using cached build stages as much as possible; `LATEST=1 ./build.sh` will also tag the last branch in the list as the `:latest` image, e.g. what you get if you just ask for `hepstore/contur`; `PUSH=1 ./build.sh` does the first version and pushes the new images and tags to DockerHub; and `FORCE=1 ./build.sh` forces rebuilds (e.g. for the master) images rather than assuming the cache is still valid just because the Dockerfile didn't change

The controlling flags can be combined, so e.g. for the `2.0.1` update, 2.0.1 was added to the list of Contur branches to use in the `contur` and `contur-herwig` `build.sh` scripts, and then first `LATEST=1 ./build.sh`, then when that was checked, `LATEST=1 PUSH=1 ./build.sh` was run to send them to DockerHub.
