#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.8
HERWIG_VERSION=7.2.3

CONTUR_BRANCHES="contur-2.4.2"
if [[ "$MAIN" = 1 ]]; then CONTUR_BRANCHES="main $CONTUR_BRANCHES"; fi

# IMPORTANT: put 'latest' version last in the list: it gets reused after the loop
for CONTUR_BRANCH in $CONTUR_BRANCHES; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}

    MSG="Building contur-herwig with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION, Herwig=$HERWIG_VERSION"
    tag="hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION"

    # TODO: master -> main?
    if [[ "$CONTUR_BRANCH" = "master" && "$FORCE" = 1 ]]; then BUILDFLAGS="--no-cache"; fi

    docker build . -f Dockerfile $BUILDFLAGS \
           --build-arg RIVET_VERSION=$RIVET_VERSION \
           --build-arg HERWIG_VERSION=$HERWIG_VERSION \
           --build-arg CONTUR_BRANCH=$CONTUR_BRANCH \
           -t $tag
    docker tag $tag hepstore/contur-herwig:$CONTUR_VERSION

    if [[ "$PUSH" = 1 ]]; then
        docker push $tag
        sleep ${SLEEP:-1}m
        docker push hepstore/contur-herwig:$CONTUR_VERSION
    fi

done

if [[ "$LATEST" = 1 ]]; then
    docker tag hepstore/contur-herwig:$CONTUR_VERSION-$HERWIG_VERSION hepstore/contur-herwig:latest
    if [[ "$PUSH" = 1 ]]; then
        sleep 30s
        docker push hepstore/contur-herwig:latest
    fi
fi
