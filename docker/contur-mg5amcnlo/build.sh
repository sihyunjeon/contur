#! /usr/bin/env bash

set -e

RIVET_VERSION=3.1.8
MG5_VERSION=3.4.2

CONTUR_BRANCHES="contur-2.4.2"
if [[ "$MAIN" = 1 ]]; then CONTUR_BRANCHES="main $CONTUR_BRANCHES"; fi

# IMPORTANT: put 'latest' version last in the list: it gets reused after the loop
for CONTUR_BRANCH in $CONTUR_BRANCHES; do
    CONTUR_VERSION=${CONTUR_BRANCH#contur-}

    MSG="Building contur-mg5amcnlo with Contur=$CONTUR_VERSION, Rivet=$RIVET_VERSION, MG5=$MG5_VERSION"
    tag="hepstore/contur-mg5amcnlo:$CONTUR_VERSION-$MG5_VERSION"

    # TODO: master -> main?
    if [[ "$CONTUR_BRANCH" = "master" && "$FORCE" = 1 ]]; then BUILDFLAGS="--no-cache"; fi

    docker build . -f Dockerfile $BUILDFLAGS \
           --build-arg RIVET_VERSION=$RIVET_VERSION \
           --build-arg MG5_VERSION=$MG5_VERSION \
           --build-arg CONTUR_BRANCH=$CONTUR_BRANCH \
           -t $tag
    docker tag $tag hepstore/contur-mg5amcnlo:$CONTUR_VERSION

    if [[ "$PUSH" = 1 ]]; then
        docker push $tag
        sleep ${SLEEP:-1}m
        docker push hepstore/contur-mg5amcnlo:$CONTUR_VERSION
    fi

done

if [[ "$LATEST" = 1 ]]; then
    docker tag hepstore/contur-mg5amcnlo:$CONTUR_VERSION-$MG5_VERSION hepstore/contur-mg5amcnlo:latest
    if [[ "$PUSH" = 1 ]]; then
        sleep ${SLEEP:-1}m
        docker push hepstore/contur-mg5amcnlo:latest
    fi
fi
