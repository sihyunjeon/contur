# -*- bash -*-

echo "-------------------------------------------------------------------------------------------"
echo "WARNING: setupContur.sh will run contur from your local repository. "
echo "It is maintained for developer use. Users should try pip install contur."

mydir="$( dirname "${BASH_SOURCE[0]}" )"
# set $CONTUR_ROOT to be the location of this file.
export CONTUR_ROOT="$( cd "$mydir" 2>&1 > /dev/null && echo "$PWD" )"
export PATH=$(echo $CONTUR_ROOT/bin:$PATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')

# setup up the user enovironment
source $CONTUR_ROOT/conturenv.sh

# override the user install so we use the local repo
export CONTUR_DATA_PATH=$CONTUR_ROOT
export CONTUR_USER_DIR=$CONTUR_ROOT/contur_user
export RIVET_DATA_PATH=$CONTUR_USER_DIR:$CONTUR_DATA_PATH/data/Rivet:$CONTUR_DATA_PATH/data/Theory
export RIVET_ANALYSIS_PATH=$CONTUR_USER_DIR:$CONTUR_DATA_PATH/data/Rivet

export PYTHONPATH=$(echo $CONTUR_ROOT:$PYTHONPATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')
export PATH=$(echo $CONTUR_ROOT/bin:$PATH | awk -v RS=':' '!a[$1]++ { if (NR > 1) printf RS; printf $1 }')
export PYTHONPATH=$PYTHONPATH:$PWD

echo "Overridden some paths for development version. Rivet paths are now:"
echo "\$RIVET_DATA_PATH=$RIVET_DATA_PATH"
echo "\$RIVET_ANALYSIS_PATH=$RIVET_ANALYSIS_PATH"

echo "\$CONTUR_DATA_PATH has been set to $CONTUR_DATA_PATH"

# This file won't exist until make has been run
ALIST=$CONTUR_USER_DIR/analysis-list
test -f $ALIST && source $ALIST

# TODO: integrate the visualiser properly, via bin/contur-visualize
chmod +x $CONTUR_ROOT/contur-visualiser/contur-visualiser

# Run some checks and reassure the user with messages if all looks good
$CONTUR_ROOT/bin/check-contur-deps -P
