To run the python tests, do `make check` in the top contur directory.

How to update the reference data
================================

If the regressions tests are failing because of a change which means
the reference data need updating, here's how to do that.

First run `make check-keep`. This will build a test directory under $CONTUR_USER_DIR and will not delete it once the tests are done. 

The commands below assume you are in your local contur repository top directory.

In the `$CONTUR_USER_DIR/tests` you'll see the files `contur_run.db`, `contur.csv`, `yodastream_results.pkl` and `Summary.txt`. Copy these to your `test/sources` directory in your contur area. Rename `Summary.txt` to `single_yoda_run.txt` and `yodastream_results.pkl` to `yodastream_results_dict.pkl`.

```
cp $CONTUR_USER_DIR/tests/contur_run.db tests/sources/
cp $CONTUR_USER_DIR/tests/contur.csv tests/sources/
cp $CONTUR_USER_DIR/tests/single_results.db tests/sources/
cp $CONTUR_USER_DIR/tests/yodastream_results.pkl tests/sources/yodastream_results_dict.pkl

```
